FROM fedora:31
EXPOSE 5000
COPY ./target/x86_64-unknown-linux-gnu/release/kundenverwaltung /opt
RUN chmod +x /opt/kundenverwaltung
CMD /opt/kundenverwaltung