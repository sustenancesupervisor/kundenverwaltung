CARGO = cargo
CROSS = cross
DOCKER = docker

build:
	@$(CROSS) build --target x86_64-unknown-linux-gnu --release

build-windows:
	@$(CROSS) build --target x86_64-pc-windows-gnu --release

build-dockerfile: build
	@$(DOCKER) build -f Dockerfile -t registry.gitlab.com/sustenancesupervisor/kundenverwaltung .

push-dockerfile: build-dockerfile
	@$(DOCKER) push registry.gitlab.com/sustenancesupervisor/kundenverwaltung
