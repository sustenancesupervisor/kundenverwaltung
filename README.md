# Kundenverwaltung

## Installation

Install [Docker](https://docs.docker.com/install/)

Install [RustUp](https://rustup.rs/)

Install Cross:
```shell script
cargo install cross
```

## Build

## Test Requests

List Users
```shell script
curl -X GET http://localhost:5000/users
```

Create User
```shell script
curl -X POST -H "Content-Type: application/json" --data "{\"first_name\":\"Leon\",\"last_name\":\"Gruenewald\",\"email\":\"l_gruenewald@web.de\",\"password\":\"asdf1234\"}" http://localhost:5000/users
```

Edit User
```shell script
curl -X PATCH -H "Content-Type: application/json" --data "{\"first_name\":\"Leo\",\"last_name\":\"Greenforest\",\"email\":\"lg@web.de\",\"password\":\"4321fdsa\"}" http://localhost:5000/users/1
```

Delete User
```shell script
curl -X DELETE http://localhost:5000/users/1
```

Login User
```
curl -X POST http://localhost:5000/users/login --data  
```
