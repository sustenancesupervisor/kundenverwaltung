use serde::{Deserialize, Serialize};
use warp::{http::Response, http::StatusCode};

#[derive(Serialize)]
pub enum ApiResultStatus<T: Serialize> {
    Success(T),
    Error(String),
}

#[derive(Deserialize)]
pub struct ApiLoginData {
    email: String,
    password: String,
}

pub struct ApiResult<T: Serialize> {
    res: ApiResultStatus<T>,
}

impl<T: Serialize> ApiResult<T> {
    pub fn new(res: ApiResultStatus<T>) -> ApiResult<T> {
        ApiResult { res }
    }
}

impl ApiLoginData {
    pub fn get_email(&self) -> String {
        self.email.clone()
    }
    pub fn get_password(&self) -> String {
        self.password.clone()
    }
}

impl<T: Serialize> From<ApiResult<T>> for Response<String> {
    fn from(res: ApiResult<T>) -> Response<String> {
        let res_builder = Response::builder().header("Content-Type", "application/json");

        return if let ApiResultStatus::Error(err) = res.res {
            res_builder
                .status(StatusCode::BAD_REQUEST)
                .body(format!(
                    "{{ \"success\": {}, \"res\": \"{}\" }}",
                    false, &err
                ))
                .unwrap()
        } else if let ApiResultStatus::Success(res_val) = res.res {
            res_builder
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(format!(
                    "{{ \"success\": {}, \"res\": {} }}",
                    true,
                    serde_json::to_string(&res_val).unwrap()
                ))
                .unwrap()
        } else {
            Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body("".to_string())
                .unwrap()
        };
    }
}
