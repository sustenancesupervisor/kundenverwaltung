use log::{error, info};
use serde::Deserialize;
use std::env;
use std::fs::File;
use std::io::ErrorKind;
use std::process::exit;

#[derive(Deserialize)]
pub struct Config {
    sqlite_path: String,
    redis_url: String,
}

impl Config {
    pub fn new() -> Box<Config> {
        let mut cfg = Config {
            redis_url: String::new(),
            sqlite_path: String::new(),
        };

        cfg.get_config_env();
        cfg.get_config_file();
        cfg.default_config();
        Box::new(cfg)
    }

    fn get_config_env(&mut self) {
        if let Ok(redis_url) = env::var("REDIS_URL") {
            self.redis_url = redis_url;
        }

        if let Ok(sqlite_path) = env::var("SQLITE_PATH") {
            self.sqlite_path = sqlite_path;
        }
    }

    fn get_config_file(&mut self) {
        if self.is_full_config() {
            return;
        };

        let maybe_config_file = File::open("./config.json");
        let config_file: File;
        let loaded_config: Config;

        if let Err(err) = &maybe_config_file {
            match err.kind() {
                ErrorKind::NotFound => {
                    info!("No config file found.");
                    return;
                }
                _ => panic!("Could not load config file: {}", err),
            }
        }

        config_file = maybe_config_file.unwrap();
        loaded_config = serde_json::from_reader(config_file).unwrap();

        if self.sqlite_path.is_empty() {
            self.sqlite_path = loaded_config.sqlite_path;
        }

        if self.redis_url.is_empty() {
            self.redis_url = loaded_config.redis_url;
        }
    }

    fn default_config(&mut self) {
        if self.redis_url.is_empty() {
            error!("REDIS_URL not found!");
            exit(1);
        }

        if self.sqlite_path.is_empty() {
            self.sqlite_path = String::from("./users.db");
        }
    }

    fn is_full_config(&self) -> bool {
        !self.sqlite_path.is_empty() && !self.redis_url.is_empty()
    }

    pub fn get_sqlite_path(&self) -> String {
        self.sqlite_path.clone()
    }

    pub fn get_redis_url(&self) -> String {
        self.redis_url.clone()
    }
}
