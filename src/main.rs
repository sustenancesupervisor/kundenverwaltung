mod api;
mod common;
mod config;
mod redis_helper;
mod routes;
mod sql_helper;
mod users;

use std::sync::{Arc, Mutex};

use log::info;
use rusqlite::Connection;
use simplelog::{LevelFilter, SimpleLogger};
use warp::Filter;

use crate::routes::*;

#[tokio::main]
async fn main() {
    let tl_result = SimpleLogger::init(LevelFilter::Info, simplelog::Config::default());

    match tl_result {
        Err(err) => {
            println!("{}:", err);
        }
        _ => {}
    }

    let config = config::Config::new();
    let sql_conn: Arc<Mutex<rusqlite::Connection>> = Arc::new(Mutex::new(
        Connection::open(config.get_sqlite_path()).unwrap(),
    ));
    let redis_client = Arc::new(Mutex::new(
        redis::Client::open(config.get_redis_url())
            .expect("Couldn't initalize redis client. Please verify your settings"),
    ));

    init_db(Arc::clone(&sql_conn)).await;
    let db = warp::any().map(move || Arc::clone(&sql_conn));
    let redis = warp::any().map(move || Arc::clone(&redis_client));

    let get_users = warp::path!("users").and(db.clone()).and_then(get_users);

    let get_users_id = warp::path!("users" / u32)
        .and(db.clone())
        .and_then(get_user);

    let post_users = warp::path!("users")
        .and(warp::body::content_length_limit(1024 * 16))
        .and(warp::body::json())
        .and(db.clone())
        .and(redis.clone())
        .and_then(create_user);

    let patch_user = warp::path!("users" / u32)
        .and(warp::body::content_length_limit(1024 * 16))
        .and(warp::body::json())
        .and(db.clone())
        .and(redis.clone())
        .and_then(edit_user);

    let delete_user = warp::path!("users" / u32)
        .and(db.clone())
        .and(redis.clone())
        .and_then(delete_user);

    let login_user = warp::path!("users" / "login")
        .and(warp::body::content_length_limit(1024 * 16))
        .and(warp::body::json())
        .and(db.clone())
        .and_then(login_user);

    let get_routes = warp::get().and(get_users.or(get_users_id));
    let post_routes = warp::post().and(post_users.or(login_user));
    let patch_routes = warp::patch().and(patch_user);
    let delete_routes = warp::delete().and(delete_user);

    let routes = warp::any().and(
        get_routes
            .or(post_routes)
            .or(patch_routes)
            .or(delete_routes),
    );

    info!("{}", "Starting server...");
    warp::serve(routes).run(([0, 0, 0, 0], 5000)).await;
}
