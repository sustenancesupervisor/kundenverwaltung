use crate::common::REDIS_PREFIX;
use log::{info, warn};
use redis::Client;
use serde::Serialize;
use std::sync::MutexGuard;

pub fn redis_publish<T: ?Sized>(
    redis_client: MutexGuard<Client>,
    public_name: &str,
    value: &T,
) -> Result<(), Box<dyn std::error::Error>>
where
    T: Serialize,
{
    let mut redis_conn = redis_client.get_connection().unwrap();
    let redis_chan: String = format!("{}{}", REDIS_PREFIX, public_name);
    let redis_response = redis::cmd("PUBLISH")
        .arg(&redis_chan)
        .arg(serde_json::to_string(value).unwrap())
        .query(&mut redis_conn);

    match redis_response {
        Ok(()) => {
            info!("[Redis]: {}", &redis_chan);
        }
        Err(err) => {
            warn!("{}", err);
        }
    }

    Ok(())
}
