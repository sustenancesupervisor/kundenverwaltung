use std::result::Result;
use std::sync::{Arc, Mutex};

use log::info;
use rusqlite::{params, Connection, OptionalExtension, NO_PARAMS};
use warp::http::Response;

use crate::api::{ApiLoginData, ApiResult, ApiResultStatus};
use crate::common::BCRYPT_COST;
use crate::redis_helper::redis_publish;
use crate::users::{EditPostUser, PostUser, ResultUser};

pub async fn init_db(db: Arc<Mutex<Connection>>) {
    info!("Initializing database...");
    let db_conn = db.lock().unwrap();
    db_conn
        .execute(
            "\
             CREATE TABLE IF NOT EXISTS user (\
             first_name 	VARCHAR NOT NULL,\
             last_name 	VARCHAR,\
             email			VARCHAR NOT NULL,\
             password_hash VARCHAR NOT NULL\
             );",
            NO_PARAMS,
        )
        .unwrap();
}

pub async fn get_users(
    db: Arc<Mutex<Connection>>,
) -> Result<Box<dyn warp::reply::Reply>, warp::Rejection> {
    let db_conn = db.lock().unwrap();
    let api_res: ApiResult<Vec<ResultUser>>;
    let warp_res: Response<String>;

    let mut stmt = db_conn
        .prepare("SELECT ROWID, first_name, last_name, email FROM user;")
        .unwrap();
    let result_users = stmt
        .query_map(NO_PARAMS, |row| Ok(ResultUser::from_row(row).unwrap()))
        .unwrap()
        .map(|db_user| db_user.unwrap())
        .collect::<Vec<ResultUser>>();

    api_res = ApiResult::new(ApiResultStatus::Success(result_users));
    warp_res = api_res.into();
    Ok(Box::new(warp_res))
}

pub async fn create_user(
    mut user: PostUser,
    db: Arc<Mutex<rusqlite::Connection>>,
    redis: Arc<Mutex<redis::Client>>,
) -> Result<Box<dyn warp::reply::Reply>, warp::Rejection> {
    let db_conn = db.lock().unwrap();
    let redis_client = redis.lock().unwrap();
    let api_res: ApiResult<u32>;
    let warp_res: Response<String>;

    if user.get_password().len() < 8 {
        api_res = ApiResult::new(ApiResultStatus::Error(
            "Password has to be at least 8 characters long".to_string(),
        ));
        warp_res = api_res.into();
        return Ok(Box::new(warp_res));
    }

    let email_count = db_conn
        .query_row(
            "SELECT count(ROWID) FROM user WHERE email = ?1;",
            params!(user.get_email()),
            |row| {
                let count: u32 = row.get(0).unwrap();
                Ok(count)
            },
        )
        .unwrap();

    if email_count != 0 {
        api_res = ApiResult::new(ApiResultStatus::Error("User already exists".to_string()));
        warp_res = api_res.into();
        return Ok(Box::new(warp_res));
    }

    db_conn
        .execute(
            "INSERT INTO user(first_name, last_name, email, password_hash) VALUES (?1, ?2, ?3, ?4)",
            params![
                user.get_first_name(),
                user.get_last_name(),
                user.get_email(),
                bcrypt::hash(user.get_password(), BCRYPT_COST).unwrap()
            ],
        )
        .unwrap();

    let user_id = db_conn
        .query_row("SELECT last_insert_rowid() FROM user;", NO_PARAMS, |row| {
            let user_id: u32 = row.get(0).unwrap();
            Ok(user_id)
        })
        .unwrap();

    let (first_name, last_name, email, _) = user.into_inner();
    let result_user = EditPostUser::new(
        Some(user_id),
        Some(first_name),
        last_name,
        Some(email),
        None,
    );

    api_res = ApiResult::new(ApiResultStatus::Success(user_id));
    warp_res = api_res.into();
    redis_publish(redis_client, "user_created", &result_user).unwrap();
    Ok(Box::new(warp_res))
}

pub async fn get_user(
    user_id: u32,
    db: Arc<Mutex<Connection>>,
) -> Result<Box<dyn warp::reply::Reply>, warp::Rejection> {
    let db_conn = db.lock().unwrap();
    let api_res: ApiResult<ResultUser>;
    let warp_res: Response<String>;

    let user = db_conn
        .query_row(
            "SELECT * FROM user WHERE ROWID = ?1;",
            params!(user_id),
            |row| Ok(ResultUser::from_row(row).unwrap()),
        )
        .unwrap();

    api_res = ApiResult::new(ApiResultStatus::Success(user));
    warp_res = api_res.into();
    Ok(Box::new(warp_res))
}

pub async fn delete_user(
    user_id: u32,
    db: Arc<Mutex<Connection>>,
    redis: Arc<Mutex<redis::Client>>,
) -> Result<Box<dyn warp::reply::Reply>, warp::Rejection> {
    let db_conn = db.lock().unwrap();
    let redis_client = redis.lock().unwrap();
    let api_res: ApiResult<String>;
    let warp_res: Response<String>;

    let res = db_conn
        .execute("DELETE FROM user WHERE ROWID = ?1;", params!(user_id))
        .unwrap();

    if res == 0 as usize {
        api_res = ApiResult::new(ApiResultStatus::Error(
            "\"User hasn't been found\"".to_string(),
        ));
        warp_res = api_res.into();
    } else {
        api_res = ApiResult::new(ApiResultStatus::Success("OK".to_string()));
        warp_res = api_res.into();
        redis_publish(redis_client, "user_deleted", &user_id).unwrap();
    }

    Ok(Box::new(warp_res))
}

pub async fn edit_user(
    user_id: u32,
    edit_data: EditPostUser,
    db: Arc<Mutex<Connection>>,
    redis: Arc<Mutex<redis::Client>>,
) -> Result<Box<dyn warp::reply::Reply>, warp::Rejection> {
    let db_conn = db.lock().unwrap();
    let redis_client = redis.lock().unwrap();
    let update_stmt = edit_data.get_sqlite_update("user", &user_id).unwrap();
    let updated_rows = db_conn
        .execute(update_stmt.get_stmt(), update_stmt.get_values())
        .unwrap();
    let api_res: ApiResult<String>;
    let warp_res: Response<String>;

    if updated_rows == 0 as usize {
        api_res = ApiResult::new(ApiResultStatus::Error("User not found".to_string()));
        warp_res = api_res.into();
    } else {
        api_res = ApiResult::new(ApiResultStatus::Success("OK".to_string()));
        warp_res = api_res.into();
        redis_publish(redis_client, "user_updated", &edit_data).unwrap();
    }

    Ok(Box::new(warp_res))
}

pub async fn login_user(
    login_data: ApiLoginData,
    db: Arc<Mutex<Connection>>,
) -> Result<Box<dyn warp::reply::Reply>, warp::Rejection> {
    let db_conn = db.lock().unwrap();
    let warp_res: Response<String>;

    let maybe_user = db_conn
        .query_row(
            "SELECT ROWID, first_name, last_name, email, password_hash FROM user WHERE email = ?1;",
            params![login_data.get_email()],
            |row| {
                Ok(EditPostUser::new(
                    Some(row.get(0).unwrap()),
                    Some(row.get(1).unwrap()),
                    Some(row.get(2).unwrap()),
                    Some(row.get(3).unwrap()),
                    Some(row.get(4).unwrap()),
                ))
            },
        )
        .optional()
        .unwrap();

    if let Some(user) = maybe_user {
        let password_verification =
            bcrypt::verify(login_data.get_password(), user.get_password().as_str());

        if let Ok(password_verification) = password_verification {
            if password_verification {
                let api_res = ApiResult::new(ApiResultStatus::Success(ResultUser::from(user)));
                warp_res = api_res.into();
                Ok(Box::new(warp_res))
            } else {
                let api_res: ApiResult<String> =
                    ApiResult::new(ApiResultStatus::Error("Invalid password".to_string()));
                warp_res = api_res.into();
                Ok(Box::new(warp_res))
            }
        } else {
            let api_res: ApiResult<String> =
                ApiResult::new(ApiResultStatus::Error("Something went wrong".to_string()));
            warp_res = api_res.into();
            Ok(Box::new(warp_res))
        }
    } else {
        let api_res: ApiResult<String> =
            ApiResult::new(ApiResultStatus::Error("User not found".to_string()));
        warp_res = api_res.into();
        Ok(Box::new(warp_res))
    }
}
