use std::collections::HashMap;
use std::string::String;

pub struct UpdateStatementValues {
    stmt: String,
    values: Vec<String>,
}

pub trait ToSQLUpdate {
    fn sqlite_update(&self, table: &str, id: &u32) -> UpdateStatementValues;
}

impl UpdateStatementValues {
    pub fn get_stmt(&self) -> &String {
        &self.stmt
    }

    pub fn get_values(&self) -> &Vec<String> {
        &self.values
    }
}

impl ToSQLUpdate for HashMap<&str, String> {
    fn sqlite_update(&self, table: &str, id: &u32) -> UpdateStatementValues {
        let mut params_iter = self.into_iter();
        let mut result: String = String::new();
        let mut ctr = 1;
        let mut values: Vec<String> = Vec::new();

        while let Some(param_value) = params_iter.next() {
            result = format!("{}{} = {}", result, param_value.0, "?");
            values.push(String::from(param_value.1));
            if self.len() != ctr {
                result = format!("{}, ", result);
            }
            ctr += 1;
        }

        UpdateStatementValues {
            stmt: format!("UPDATE {} SET {} WHERE ROWID = {}", table, result, id),
            values,
        }
    }
}
