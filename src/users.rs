use crate::common::BCRYPT_COST;
use crate::sql_helper::{ToSQLUpdate, UpdateStatementValues};
use rusqlite::Row;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct PostUser {
    first_name: String,
    last_name: Option<String>,
    email: String,
    password: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct EditPostUser {
    id: Option<u32>,
    first_name: Option<String>,
    last_name: Option<String>,
    email: Option<String>,
    password: Option<String>,
}

#[derive(Serialize)]
pub struct ResultUser {
    id: u32,
    first_name: String,
    last_name: Option<String>,
    email: String,
}

impl PostUser {
    pub fn get_first_name(&mut self) -> String {
        return self.first_name.clone();
    }

    pub fn get_last_name(&mut self) -> String {
        if self.last_name.is_none() {
            return String::new();
        }

        return self.last_name.as_ref().unwrap().clone();
    }

    pub fn get_email(&mut self) -> String {
        return self.email.clone();
    }

    pub fn get_password(&mut self) -> String {
        return self.password.clone();
    }

    pub fn into_inner(self) -> (String, Option<String>, String, String) {
        (self.first_name, self.last_name, self.email, self.password)
    }
}

impl EditPostUser {
    pub(crate) fn new(
        id: Option<u32>,
        first_name: Option<String>,
        last_name: Option<String>,
        email: Option<String>,
        password: Option<String>,
    ) -> EditPostUser {
        EditPostUser {
            id,
            first_name,
            last_name,
            email,
            password,
        }
    }

    pub(crate) fn get_sqlite_update(
        &self,
        table: &str,
        id: &u32,
    ) -> Result<UpdateStatementValues, ()> {
        let mut params: HashMap<&str, String> = HashMap::new();

        if let Some(first_name) = &self.first_name {
            params.insert("first_name", first_name.clone());
        }

        if let Some(last_name) = &self.last_name {
            params.insert("last_name", last_name.clone());
        }

        if let Some(email) = &self.email {
            params.insert("email", email.clone());
        }

        if let Some(password) = &self.password {
            params.insert(
                "password_hash",
                bcrypt::hash(password, BCRYPT_COST).unwrap(),
            );
        }

        Ok(params.sqlite_update(table, id))
    }

    pub(crate) fn get_password(&self) -> String {
        self.password.clone().unwrap()
    }
}

impl ResultUser {
    pub(crate) fn from_row(row: &Row) -> Result<ResultUser, Box<dyn std::error::Error>> {
        Ok(ResultUser {
            id: row.get(0)?,
            first_name: row.get(1)?,
            last_name: row.get(2)?,
            email: row.get(3)?,
        })
    }
}

impl From<EditPostUser> for ResultUser {
    fn from(user: EditPostUser) -> ResultUser {
        let EditPostUser {
            id,
            first_name,
            last_name,
            email,
            password: _,
        } = user;

        ResultUser {
            id: id.unwrap(),
            first_name: first_name.unwrap(),
            last_name,
            email: email.unwrap(),
        }
    }
}
